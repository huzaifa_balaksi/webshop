<?php
include '../private/connection.php';

if (isset($_POST['submit'])){
    $firstname = $_POST['voornaam'];
    $lastname = $_POST['achternaam'];
    $live = $_POST['woonplaats'];
    $street = $_POST['straat'];
    $housenummber = $_POST['huisnummer'];
    $post = $_POST['postcode'];
    $email = $_POST['email'];
    $password =  $_POST['wachtwoord'];
    $dateofbirth = $_POST['geboortedatum'];

    $sql= "INSERT INTO `login`( voornaam, achternaam, woonplaats, straat, huisnummer, postcode, email, wachtwoord, geboortedatum, `role`) 
            VALUES (:firstname,:lastname,:live,:street,:housenummber,:post,:email , :password,:dateofbirth, 'klant')";

    $stmt = $conn ->prepare($sql);
    $stmt->execute(array(
        ':firstname'=> $firstname,
        ':lastname'=>$lastname,
        ':live'=>$live,
        ':street'=>$street,
        ':housenummber'=>$housenummber,
        ':post'=>$post,
        ':email'=>$email,
        ':password'=>$password,
        ':dateofbirth'=>$dateofbirth
    ));
    header('Location: ../index.php?page=login');
}