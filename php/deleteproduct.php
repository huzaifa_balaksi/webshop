<?php
include '../private/connection.php';

$bID = $_POST['productid'];


$sql = "DELETE FROM `producten` WHERE `productid` = :productid";
$res = $conn->prepare($sql);
$res->bindParam(':productid', $bID);
$res->execute();
header('location: ../index.php?page=menuadmin');