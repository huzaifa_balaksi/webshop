<?php
include '../private/connection.php';
$productid = $_POST['productid'];
$naam = $_POST['naam'];
$prijs = $_POST['prijs'];
$beschrijven = $_POST['beschrijven'];
$image = base64_encode(file_get_contents($_FILES['filename']['tmp_name']));
$sql = "UPDATE producten SET naam = :naam , prijs = :prijs , beschrijven = :beschrijven , img = :image WHERE productid = :productid";
$stmt = $conn->prepare($sql);
$stmt->execute(array(
   ':productid'=> $productid,
    ':naam' => $naam,
    ':prijs'=>$prijs,
    ':beschrijven' => $beschrijven,
    ':image' => $image
));
 header('Location: ../index.php?page=menuadmin');