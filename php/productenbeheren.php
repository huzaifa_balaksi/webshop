<?php
include '../private/connection.php';
if (isset($_POST['submit'])) {
    $name = $_POST['naam'];
    $price = $_POST['prijs'];
    $beschrijven = $_POST['beschrijven'];
    $categorie = $_POST['categorie'];
    $image = base64_encode(file_get_contents($_FILES['filename']['tmp_name']));

    $sql = "INSERT INTO producten(naam , prijs, categorie, beschrijven,img) VALUE (:naam , :prijs, :categorie, :beschrijven,:image)";

    $stmt = $conn->prepare($sql);
    $stmt->execute(array(
        ':naam' => $name,
        ':prijs' => $price,
        ':beschrijven'=> $beschrijven,
        ':categorie' => $categorie,
         ':image'=> $image
    ));
    header('Location: ../index.php?page=menuadmin');

}
?>



