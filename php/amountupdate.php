<?php
include "../private/connection.php";
session_start();
$productid = $_POST['productid'];
$userid = $_SESSION['userid'];
$amount = $_POST['amount'];

    $sql3 = "UPDATE winkelmandje SET amount = :amount  WHERE productid = :productid AND userid = :userid";
    $smt3 = $conn->prepare($sql3);
    $smt3->execute(array(
        ':productid' => $productid,
        ':userid' => $userid,
        ':amount' => $amount
    ));
header('location: ../index.php?page=winkelmandje');
