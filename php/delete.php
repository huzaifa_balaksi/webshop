<?php
include '../private/connection.php';

$bID = $_POST['id'];

$sql = "DELETE FROM `categorie` WHERE `id` = :id";
$res = $conn->prepare($sql);
$res->bindParam(':id', $bID);
$res->execute();
header('location: ../index.php?page=menuadmin');