<?php
include '../private/connection.php';
session_start();
$amount = $_POST['amount'];




// ophalen gegevens uit winkelmandje
$sql ="SELECT w.productid , userid , amount , prijs FROM winkelmandje W 
 INNER JOIN producten p ON p.productid = w.productid WHERE userid = :userid";
$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ':userid' => $_SESSION['userid'],
));



// order aanmaken
$datum = date("Y-m-d");
$sql2 = "iNSERT INTO `order` (userid, datum) VALUES (:userid, :datum)";
$stmt2 = $conn->prepare($sql2);
$stmt2->execute(array(
    ':datum' => $datum,
    ':userid' => $_SESSION['userid']

));

$orderid = $conn->lastInsertId();

// producten uit winkelmandje toevoegen aan de order
while($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

    $sql3 = "INSERT INTO orderproduct(productid, orderid , amount, price) VALUE (:productid , :orderid, :amount, :price)";
    $stmt3 = $conn->prepare($sql3);
    $stmt3->execute(array(
        ':orderid' => $orderid,
        ':productid' => $result['productid'],
        ':amount' => $result['amount'],
        ':price' => $result['prijs']
    ));
}

// leeggooien winkelmandje
$userid = $_SESSION['userid'];
$sql = "DELETE FROM winkelmandje WHERE userid = $userid ";
$query = $conn->prepare($sql);
$query->execute();

header('Location: ../index.php?page=winkelmandje');
