<?php
include "../private/connection.php";
$productid = $_POST['productid'];
$sql = "DELETE FROM winkelmandje WHERE productid=:productid";
$stmt = $conn->prepare($sql);
$stmt->bindParam(':productid', $productid);
$stmt->execute();
header('location:../index.php?page=winkelmandje');
