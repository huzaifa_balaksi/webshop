<?php
require 'private/connection.php';

$sql = "Select * from categorie";
$stmt = $conn->prepare($sql);
$stmt->execute();
?>
<form action="php/productenbeheren.php" method="post" enctype="multipart/form-data">
    <div class="registratie-box">
        <h1 class="reg">product toevoegen</h1>
        <div class="registratie-box">
            <input type="text" placeholder="naam" name="naam">
            <input type="text" placeholder="prijs" name="prijs"><br>
            <input type="text" placeholder="beschrijven" name="beschrijven"><br>
            <input type="file" id="myFile" name="filename">
            <label for="img">kies een categorie:</label>
            <select name="categorie">
                <?php while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) { ?>
                    <option value="<?= $result['id'] ?>"><?= $result['naam']?></option>
                <?php } ?>
            </select>
            <input class="btn" type="submit" name="submit" value="toevoegen">
        </div>
</form>