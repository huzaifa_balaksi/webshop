<div class="balk">
    <?php

    if (isset($_SESSION['role']) ){
        if($_SESSION['role']=='admin') {
            $menuItems = array(

                array('menuadmin','menuadmin'),
                array('about','about'),
                array('productbeheren','productbeheren'),
                array('winkelmandje','winkelmandje'),
                array('logout','logout')

            );
        } elseif($_SESSION['role']=='klant') {
            $menuItems = array(

                array('menuklant','menuklant'),
                array('about','about'),
                array('winkelmandje','winkelmandje'),
                array('bestelgeschiedenis','bestelgeschiedenis'),
                array('logout','logout')
            );
        }
    } else {
        $menuItems = array(
            array('login', 'login'),
            array('registratiepagina', 'registratiepagina')
        );
    }
    foreach ($menuItems as $menuItem) {
        echo '<a href="index.php?page='.$menuItem[1].'">'.$menuItem[0].'</a>';
    }
    ?>
</div>
